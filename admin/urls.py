from django.contrib import admin
from django.contrib.auth import urls
from django.urls import path, include
from clients import urls as clients_urls
from products import urls as products_urls
from sales import urls as sales_urls
from home import urls as home_urls

# Import library pra conf de login
from django.contrib.auth import views as auth_views

## Conf para exibição de imagens no browser, conf também no final da urlpatterns
from django.conf import settings
from django.conf.urls.static import static


import debug_toolbar


urlpatterns = [

    path('', include(home_urls)),
    path('clients/', include(clients_urls)),
    path('products/', include(products_urls)),
    path('sales/', include(sales_urls)),
    path('admin/', admin.site.urls),
    path('login/', auth_views.LoginView.as_view(), name='login'),
    path('__debug__/', include(debug_toolbar.urls)),
    path('', include('django.contrib.auth.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


# Custom Label do módulo Admin
admin.site.site_header = 'Gestão de Clientes'
admin.site.index_title = 'Administração'
admin.site.site_title = 'Ver.1.0'