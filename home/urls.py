from django.urls import path
from django.views.generic import TemplateView

from .views import home, my_logout, MyView
from .views import HomePageView



urlpatterns = [
    path('', home, name="home"),
    path('logout/', my_logout, name='logout'),
    path('home/', home, name="home"),
    path('home2/', TemplateView.as_view(template_name='home/home2.html')),
    path('home3/', HomePageView.as_view(template_name='home/home3.html')),
    path('home4/', MyView.as_view()),
]