from django.shortcuts import render, redirect, render_to_response
from django.contrib.auth import logout

# Create your views here.
from django.views import View
from django.views.generic import TemplateView
from django.http import HttpResponse


def home(request):
    #import pdb; pdb.set_trace()
    a = 20
    b = 30
    c = a * b

    return render(request, 'home/home.html', {'result': c})


def my_logout(request):
    logout(request)
    return redirect('home')


class HomePageView(TemplateView):
    template_name = "home/home3.html"
    
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        
        nome = 'dacio lima'
        a = 4
        b = 6
        res = a + b

        context['aviso'] = res

        return context

class MyView(View):

    def get(self, request, *args, **kwargs):
        response = render_to_response('home/home4.html')
        response.set_cookie('color', 'blue', max_age=1000)
        mycookie = request.COOKIES.get('color')
        print(mycookie)


        return response
    
    def post(self, request, *args, **kwargs):
        return HttpResponse('post')

