from debug_toolbar.panels import logging
from django.http import HttpResponse
from django.shortcuts import render, redirect

# Create your views here.
from django.utils.datetime_safe import datetime
from django.views import View

from sales.forms import ItemPedidoForm, ItemDoPedidoModelForm
from sales.models import Sale, ItemDoPedido

# Gerando log em arquivo
import logging


logger = logging.getLogger('django')


class DashboardView(View):

    # Valida permissão atraves do override no dispatch, visto que o mesmo é executado antes de qualquer view.
    def dispatch(self, request, *args, **kwargs):
        if not request.user.has_perm('sales.ver_dashboard'):
            return HttpResponse('Acesso Negado, usuário sem permissão')

        return super(DashboardView, self).dispatch(request, *args, **kwargs)

    def get(self, request):

        data = {
                'media': Sale.objects.media(),
                'media_desc': Sale.objects.media_desc(),
                'min': Sale.objects.minimo(),
                'max': Sale.objects.maximo(),
                'qtd_ped': Sale.objects.qtd_ped(),
                'qtd_ped_nfe': Sale.objects.qtd_ped_nfe()}

        return render(request, 'sales/dashboard.html', data)


class ListaSales(View):
    def get(self, request):

        logger.debug('Acessaram a lista de Sales')
        time = datetime.now()
        logger.exception(time.strftime("%Y-%m-%d %H:%M:%S") + ' ' + str(request.user))


        sales = Sale.objects.all()
        count_sales = sales.count()
        return render(request, 'sales/lista-sales.html', {'sales': sales, 'count_sales': count_sales})



class NovoPedido(View):
    def get(self, request):
        return render(request, 'sales/novo-pedido.html')

    def post(self, request):
        data = {}
        data['form_item'] = ItemPedidoForm()
        data['num_sale'] = request.POST['num_sale']
        data['desconto'] = float(request.POST['desconto'].replace(',', '.'))
        data['sale_id'] = request.POST['sale_id']

        if data['sale_id']:
            sale = Sale.objects.get(id=data['sale_id'])
            sale.desconto = data['desconto']
            sale.num_sale = data['num_sale']
            sale.save()
        else:
            sale = Sale.objects.create(
                num_sale=data['num_sale'], desconto=data['desconto'])

        itens = sale.itemdopedido_set.all()
        data['sale_obj'] = sale
        data['itens'] = itens
        return render(
            request, 'sales/novo-pedido.html', data)


class NovoItemPedido(View):
    def get(self, request, pk):
        pass

    def post(self, request, sale):
        data = {}

        item = ItemDoPedido.objects.filter(product_id=request.POST['product_id'], sale_id=sale)
        if item:
            data['message'] = 'Item já foi incluido no pedido, favor editar item se necessário'
            item = item[0]
        else:
            item = ItemDoPedido.objects.create(
                product_id=request.POST['product_id'], quantidade=request.POST['quantidade'],
                desconto=request.POST['desconto'], sale_id=sale)

        data['item'] = item
        data['form_item'] = ItemPedidoForm()
        data['num_sale'] = item.sale.num_sale
        data['desconto'] = item.sale.desconto
        data['sale_obj'] = item.sale
        data['itens'] = item.sale.itemdopedido_set.all()

        return render(
            request, 'sales/novo-pedido.html', data)


class EditPedido(View):
    def get(self, request, sale):
        data = {}
        sale = Sale.objects.get(id=sale)
        data['form_item'] = ItemPedidoForm()
        data['num_sale'] = sale.num_sale
        data['desconto'] = sale.desconto
        data['sale_obj'] = sale
        data['itens'] = sale.itemdopedido_set.all()

        return render(request, 'sales/novo-pedido.html', data)


class DeletePedido(View):
    def get(self, request, sale):
        sale = Sale.objects.get(id=sale)
        return render(request, 'sales/delete-pedido-confirm.html', {'sales': sale})


    def post(self, request, sale):
        sale = Sale.objects.get(id=sale)
        sale.delete()
        return redirect('lista_sales_cbv')


class DeleteItemPedido(View):
    def get(self, request, item):
        item = ItemDoPedido.objects.get(id=item)
        return render(request, 'sales/delete-item-pedido-confirm.html', {'item': item})

    def post(self, request, item):
        item_sale = ItemDoPedido.objects.get(id=item)
        sale_id = item_sale.sale.id
        item_sale.delete()
        return redirect('edit_pedido_cbv', sale=sale_id)

class EditItemPedido(View):
    def get(self, request, item):
        item_pedido = ItemDoPedido.objects.get(id=item)
        form = ItemDoPedidoModelForm(instance=item_pedido)
        return render(request, 'sales/edit-itempedido.html', {'item': item_pedido, 'form': form})

    def post(self, request, item):
        item_pedido = ItemDoPedido.objects.get(id=item)
        item_pedido.quantidade = request.POST['quantidade']
        item_pedido.desconto = request.POST['desconto']
        item_pedido.save()
        sale_id = item_pedido.sale.id
        return redirect('edit_pedido_cbv', sale=sale_id)