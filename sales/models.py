from django.db import models
from django.db.models import Sum, FloatField, F
from django.db.models.signals import post_save
from django.dispatch import receiver

from clients.models import Person
from products.models import Product


# Modelo Sale
from sales.managers import SaleManager


class Sale(models.Model):
    ABERTA = 'AB'
    FECHADA = 'FH'
    PROCESSANDO = 'PS'
    DESCONHECIDO = 'DC'
    STATUS = (
        (ABERTA, 'Aberta'),
        (FECHADA, 'Fechada'),
        (PROCESSANDO, 'Processando'),
        (DESCONHECIDO, 'Desconhecido'),

    )

    num_sale = models.CharField(max_length=7)
    valor = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True)
    desconto = models.DecimalField(max_digits=5, decimal_places=2, default=0)
    imposto = models.DecimalField(max_digits=5, decimal_places=2, default=0)
    person = models.ForeignKey(Person, null=True, blank=True, on_delete=models.PROTECT)
    nfe_emitida = models.BooleanField(default=False)
    status = models.CharField(choices=STATUS, default=DESCONHECIDO, max_length=2)



    # Conf para uso de managers nesse model
    objects = SaleManager()

    class Meta:
        permissions = (
            ('setar_nef', 'Usuário terá permissão para Setar NFE'),
            ('ver_dashboard', 'Usuário tem permissão Dashboard'),
        )



    def calcular_total(self):

        tot = self.itemdopedido_set.all().aggregate(
            tot_ped=Sum((F('quantidade') * F('product__preco')) - F('desconto'), output_field=FloatField())
        )['tot_ped'] or 0

        tot = tot - float(self.imposto) - float(self.desconto)
        self.valor = tot
        Sale.objects.filter(id=self.id).update(valor=tot)


    def __str__(self):
        return self.num_sale


class ItemDoPedido(models.Model):
    sale = models.ForeignKey(Sale, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantidade = models.FloatField()
    desconto = models.DecimalField(max_digits=5, decimal_places=2)

    def __str__(self):
        return self.sale.num_sale + ' ' + self.product.description

    class Meta:
        unique_together = (
            ("sale", "product")
        )


@receiver(post_save, sender=ItemDoPedido)
def update_sale_total(sender, instance, **kwargs):
    instance.sale.calcular_total()


@receiver(post_save, sender=Sale)
def update_sale_total2(sender, instance, **kwargs):
    instance.calcular_total()



