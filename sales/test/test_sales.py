from django.test import TestCase

from products.models import Product
from sales.models import Sale, ItemDoPedido


class SaleTestCase(TestCase):
    def setUp(self):
        self.sale = Sale.objects.create(num_sale=123, desconto=10, status='AB')
        self.product = Product.objects.create(description='Banana', preco=10)

    def test_qtd_sales(self):
        """Testa a quantidade de venda criada"""
        assert Sale.objects.all().count() == 1

    def test_valor_sale(self):
        """Testa valor total venda"""
        ItemDoPedido.objects.create(
            sale=self.sale, product=self.product, quantidade=5, desconto=10)
        assert self.sale.valor == 30

    def test_desconto(self):
        """Testa retorno correto do desconto"""
        assert self.sale.desconto == 10

    def test_item_include_list_pedido(self):
        """Testa se item foi incluido no pedido"""
        item = ItemDoPedido.objects.create(
            sale=self.sale, product=self.product, quantidade=5, desconto=10)
        self.assertIn(item, self.sale.itemdopedido_set.all())

    def test_checa_nfe_false(self):
        """Testa se nfe está false"""
        self.assertFalse(self.sale.nfe_emitida)

    def test_checa_status(self):
        self.sale.status = 'PC'
        self.sale.save()
        self.assertEqual(self.sale.status, 'PC')
