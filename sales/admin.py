from django.contrib import admin

from .actions import nfe_emitida, nfe_nao_emitida
from sales.models import ItemDoPedido, Sale



class ItemDoPedidoInline(admin.TabularInline):
    model = ItemDoPedido
    extra = 1


#Criar filtros dentro do Model

class SaleAdmin(admin.ModelAdmin):
    readonly_fields = ('valor',)
    autocomplete_fields = ['person']
    #raw_id_fields = ('person',)
    list_filter = ('person__doc', 'desconto')
    list_display = ('id', 'person', 'nfe_emitida')
    search_fields = ('id','person__first_name', 'person__doc__num_doc')
    inlines = [ItemDoPedidoInline]

    # Ações no admin quando caixas estiverem marcadas
    actions = [nfe_emitida, nfe_nao_emitida]

    # Filtro para escolha de itens
    #filter_horizontal = ['itens',]



    def total(self, obj):
        return obj.get_total()


    total.short_description = 'Total'



admin.site.register(ItemDoPedido)
admin.site.register(Sale, SaleAdmin)