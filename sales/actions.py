from django.http import HttpResponseNotFound


def nfe_emitida(modelAdmin, request, queryset):
    if request.user.has_perm('sales.setar_nef'):
        queryset.update(nfe_emitida=True)
    else:
        return HttpResponseNotFound('<h1>Sem autorização</h1>')



nfe_emitida.short_description = "Nfe emitidas"


def nfe_nao_emitida(modelAdmin, request, queryset):
    queryset.update(nfe_emitida=False)


nfe_nao_emitida.short_description = "Nfe não emitidas"
