from django import forms
from sales.models import ItemDoPedido


# Criando form com o Form do Django
class ItemPedidoForm(forms.Form):
    product_id = forms.CharField(label='ID do Produto', max_length=100)
    quantidade = forms.IntegerField(label='Quantidade')
    desconto = forms.DecimalField(label='Desconto', max_digits=7, decimal_places=2)


# Criando form com o ModelForm
class ItemDoPedidoModelForm(forms.ModelForm):
    class Meta:
        model = ItemDoPedido
        # Coringa all mostra todos os campos do form
        #fields = '__all__'
        fields = ['quantidade', 'desconto']



