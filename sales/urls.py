from django.urls import path
from .views import DashboardView, NovoPedido, NovoItemPedido, ListaSales, EditPedido, DeletePedido, DeleteItemPedido, \
    EditItemPedido

urlpatterns = [
   path('dashboard/', DashboardView.as_view(), name="dashboard_cbv"),
   path('novopedido/', NovoPedido.as_view(), name="novo_pedido_cbv"),
   path('', ListaSales.as_view(), name="lista_sales_cbv"),
   path('novo-item-pedido/<int:sales>/', NovoItemPedido.as_view(), name="novo_item_pedido_cbv"),
   path('edit-pedido/<int:sales>/', EditPedido.as_view(), name="edit_pedido_cbv"),
   path('delete-pedido/<int:sales>/', DeletePedido.as_view(), name="delete_pedido_cbv"),
   path('delete-item-pedido/<int:item>/', DeleteItemPedido.as_view(), name="delete_item_pedido_cbv"),
   path('edit-item-pedido/<int:item>/', EditItemPedido.as_view(), name="edit_item_pedido_cbv"),

]
