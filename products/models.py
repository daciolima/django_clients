from django.db import models


# Modelo Product
class Product(models.Model):
    description = models.CharField(max_length=50)
    preco = models.DecimalField(max_digits=5, decimal_places=2)


    def __str__(self):
        return str(self.id) + ' - ' + self.description + ' - ' + str(self.preco)
