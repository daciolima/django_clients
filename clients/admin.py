from django.contrib import admin

from .models import Document
from .models import Person

class PersonAdmin(admin.ModelAdmin):

    fieldsets = (
        ('Pessoais', {'fields': ('first_name', 'last_name', 'doc')}),

        ('Dados', {
            'classes': ('collapse',),
            'fields': ('age', 'salary', 'bio', 'photo')})

    )


    #fields = (('first_name', 'last_name'), 'age', 'salary', 'bio', 'photo', 'doc')
    #exclude = ('bio')
    list_display = ('first_name', 'last_name', 'age', 'salary', 'bio', 'tem_foto', 'doc')

    def tem_foto(self, obj):
        if obj.photo:
            return 'Possui'
        else:
            return 'Não possui'


    list_filter = ('age', 'salary')

    # Base para o auto-complete no Model Sale
    search_fields = ['id', 'first_name']

    autocomplete_fields = ['doc']




class DocumentAdmin(admin.ModelAdmin):
    search_fields = ['num_doc']



# Registra seus models de forma que apareçam no admin
admin.site.register(Document, DocumentAdmin)
admin.site.register(Person, PersonAdmin)


