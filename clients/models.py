from django.core.mail import send_mail, mail_admins, send_mass_mail
from django.db import models
from django.template.loader import render_to_string

# Modelo Document
class Document(models.Model):
    num_doc = models.CharField(max_length=50)
    
    def __str__(self):
        return self.num_doc


# Modelo Client
class Person(models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    age = models.IntegerField()
    salary = models.DecimalField(max_digits=7, decimal_places=2)
    bio = models.TextField()
    photo = models.ImageField(upload_to='clients_photos', null=True, blank=True)
    doc = models.OneToOneField(Document, null=True, blank=True, on_delete=models.CASCADE)

    class Meta:

        #Chave composta - Dois nomes iguais não podem ser atribuídos a um mesmo doc
        unique_together = (("first_name", "doc"), )

        # Criando permissões específicas para tratamento de autorização
        permissions = (
            ('deletar_clients', 'Usuário tem permissão deletar clientes'),
        )

    @property
    def nome_completo(self):
        return self.first_name + " " + self.last_name


    # Override do método save para enviar email assim que salvar usuário
    def save(self, *args, **kwargs):
        super(Person, self).save(**args, **kwargs)

        # Code email para cada novo usuário cadastrado
        data = { 'cliente': self.first_name }
        plain_text = render_to_string('clients/email/novo_client.txt', data)
        html_email = render_to_string('clients/email/novo_client.html', data)
        send_mail(
            'Novo Usuário Cadastrado',
            plain_text,
            'from:contato.dacio@gmail.com',
            ['to:contato.dacio@gmail.com'],
            html_message=html_email,
            fail_silently=False

        )

        # Code para envio de email para os admins da aplicação
        mail_admins(
            'Novo usuário cadastrado',
            plain_text,
            html_message=html_email,
            fail_silently=False,

        )

        # Code para envio em massa de email pela aplicação
        message1 = ('Título', 'Texto', 'from@email.com', ['to:emai1-1@email.com', 'emai1-2@email.com', ])
        message2 = ('Título', 'Texto', 'from@email.com', ['to:emai1-1@email.com', ])
        send_mass_mail((message1, message2), fail_silently=False)

    def __str__(self):
        return str(self.id) + ' ' + self.first_name + ' ' + self.last_name
    


