from random import randrange

from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.forms import model_to_dict
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.urls import reverse_lazy

from .models import Person
from sales.models import Sale
from products.models import Product
from .forms import PersonForm

from django.views.generic import View
from django.views.generic import ListView, UpdateView, DeleteView
from django.views.generic import CreateView
from django.views.generic.detail import DetailView
from django.utils import timezone


# Function BasedView - FBV.
@login_required
def persons_list(request):
    nome = request.GET.get('nome', None)
    sobrenome = request.GET.get('sobrenome', None)

    if nome or sobrenome:
        persons = Person.objects.filter(first_name__icontains=nome) | Person.objects.filter(last_name__icontains=sobrenome)

        
    else:
        persons = Person.objects.all()
    
    return render(request, 'person.html', {'persons': persons})


@login_required
def persons_new(request):

    if not request.user.has_perm('clients.add_person'):
        return HttpResponse('Não autorizado!')
    elif not request.user.is_superuser:
        return HttpResponse('Usuário não é ROOT')

    form = PersonForm(request.POST or None, request.FILES or None)

    if form.is_valid():
       form.save() 
       return redirect('person_list')
    else:
        return render(request, 'person_form.html', {'form': form})

@login_required
def persons_update(request, id):
    person = get_object_or_404(Person, pk=id)
    form = PersonForm(request.POST or None, request.FILES or None, instance=person)
    if form.is_valid():
        form.save()
        return redirect('person_list')
    else:
        return render(request, 'person_form.html', {'form': form}) 

@login_required
def persons_delete(request, id):
    person = get_object_or_404(Person, pk=id)

    if request.method == 'POST':
        person.delete()
        return redirect('person_list')
    return render(request, 'person_delete_conf.html', {'person': person})


# Class Based View - CBV
class PersonList(LoginRequiredMixin, ListView):
    model = Person


    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        primeiro_acesso = self.request.session.get('Primeiro_acesso', False)
        if not primeiro_acesso:
            context['message'] = 'Seja bem vindo ao seu promeiro acesso'
            self.request.session['Primeiro Acesso'] = True
        else:
            context['message'] = 'Você já acessou hoje!'

        return context


class PersonDetail(LoginRequiredMixin, DetailView):
    model = Person


    # Override nesse método retornando o cliente e ao mesmo tempo o doc relacionado a este.
    def get_object(self, queryset=None):
        pk = self.kwargs.get(self.pk_url_kwarg)
        return Person.objects.select_related('doc').get(id=pk)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['now'] = timezone.now()
        context['vendas'] = Sale.objects.filter(person_id=self.object.id)
        return context




class PersonCreate(LoginRequiredMixin, CreateView):
    model = Person
    fields = ['first_name','last_name','age', 'salary', 'bio','photo']
    #success_url = '/clients/person_list'

    # método onde vc pode trabalha e até mesmo injetar contexto antes de redirecionar
    def get_success_url(self):
        return reverse_lazy('person_list_cbv')


class PersonUpdate(LoginRequiredMixin, UpdateView):
    model = Person
    fields = ['first_name','last_name','age', 'salary', 'bio','photo']
    success_url = reverse_lazy('person_list_cbv')


class PersonDelete(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):

    permission_required = ('clients.deletar_clients',)
    model = Person
    success_url = reverse_lazy('person_list_cbv')


class ProductBulk(View):
    def get(self, request):
        products = ['Banana', 'Maça', 'Laranja', 'Pêra', 'Goiaba', 'Manga', 'Cajá', 'Melão', 'Limão', 'Tangerina', 'Macaxeira']
        list_products = []

        for product in products:
            p = Product(description=product,  preco=10)
            list_products.append(p)

        Product.objects.bulk_create(list_products)

        return HttpResponse('Base de itens carregada')


class ClientBulk(View):
    def get(self, request):
        clients = ['Isaac', 'Maurício', 'Lauro', 'Marta', 'Adriana', 'Bruna', 'Dácio', 'André', 'Joana', 'Marcelo', 'João']
        list_clients = []

        for client in clients:
            p = Person(first_name=client)
            list_clients.append(p)



        Person.objects.bulk_create(list_clients)

        return HttpResponse('Base de clients carregada')


def api(request):

    produtos = Product.objects.all()

    l = []
    for produto in produtos:
        l.append(model_to_dict(produto))

    return JsonResponse(l, status=200, safe=False)

class ApiCbv(View):
    def get(self, request):
        produtos = Product.objects.all()

        l = []
        for produto in produtos:
            l.append(model_to_dict(produto))

        return JsonResponse(l, status=200, safe=False)